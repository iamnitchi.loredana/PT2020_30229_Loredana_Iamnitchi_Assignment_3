package businesslogic;

import databaseaccess.ClientDAO;
import databaseaccess.OrderDAO;
import databaseaccess.OrderItemDAO;
import exception.DeletionFailedException;
import exception.InsertionFailedException;
import model.Client;

/**
 * Aceasta clasa se ocupa cu logica operatiilor folosite pe tabela Clients, mai exact insert si delete.
 */

public class ClientBLL {
    /**
     * Aceasta metoda are rolul de a insera un client apeland metoda corespunzatoare din ClientDAO. Totodata se verifica daca
     * inserarea are loc cu succes, in caz contrar se va arunca o exceptie de tipul InsertionFailedException.
     * @param client clientul care urmeaza a fi inserat
     * @throws InsertionFailedException
     */
    public static void insertClient(Client client) throws InsertionFailedException
    {
        int success=ClientDAO.insertClient(client);
        if(success != 1) {
            throw new InsertionFailedException("Inserting client "+client.getName()+" -failed.");
        }
    }

    /**
     * Aceasta metoda are rolul de a sterge un client apeland metoda corespunzatoare din ClientDAO. Odata cu stergerea
     * unui clientului se vor sterge toate comenzile acestuia atat din OrderItem, cat si din Orders. In cazul in care
     * stergerea clientului nu are loc cu succes, se va arunca o exceptie de tipul DeletionFailedException.
     * @param name numele clientului
     * @param address adresa clientului
     * @throws DeletionFailedException
     */
    public static void deleteClient(String name, String address) throws DeletionFailedException {
        int id_client=ClientDAO.findByName(name);
        int success1= OrderItemDAO.deleteOrderItemClient(id_client);
        int success2= OrderDAO.deleteOrderClient(id_client);
        int success = ClientDAO.deleteClient(name, address);
        if (success != 1) {
            throw new DeletionFailedException("Deleting client "+name+" -failed.");
        }
        if (success1 != 1 || success2!=1) {
            throw new DeletionFailedException("Deleting client orders -failed.");
        }
    }
}
