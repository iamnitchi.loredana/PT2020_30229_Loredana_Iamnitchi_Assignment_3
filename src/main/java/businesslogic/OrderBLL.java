package businesslogic;

import databaseaccess.OrderDAO;
import exception.InsertionFailedException;
import model.Order;

/**
 * Aceasta clasa se ocupa cu logica operatiilor folosite pe tabela Orders, mai exact insert.
 */

public class OrderBLL {

    /**
     * Aceasta metoda are rolul de a insera o comanda apeland metoda corespunzatoare din OrderDAO. Totodata se verifica daca
     * inserarea are loc cu succes, in caz contrar se va arunca o exceptie de tipul InsertionFailedException.
     * @param o comanda cre urmeaza a fi inserata
     * @throws InsertionFailedException
     */

    public static void insertOrder(Order o) throws InsertionFailedException {
        int success= OrderDAO.insertOrder(o);
        if(success != 1) {
            throw new InsertionFailedException("Inserting order -failed");
        }
    }

}
