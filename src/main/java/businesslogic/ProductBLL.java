package businesslogic;

import databaseaccess.OrderDAO;
import databaseaccess.OrderItemDAO;
import databaseaccess.ProductDAO;
import exception.DeletionFailedException;
import exception.InsertionFailedException;
import exception.UpdateFailedException;
import model.Product;

/**
 * Aceasta clasa se ocupa cu logica operatiilor folosite pe tabela Products, mai exact insert, delete si update.
 */

public class ProductBLL {

    /**
     *  Aceasta metoda are rolul de a insera un produs apeland metoda corespunzatoare din ProductDAO. Totodata se verifica daca
     *  inserarea are loc cu succes, in caz contrar se va arunca o exceptie de tipul InsertionFailedException.
     * @param p produsul care urmeaza a fi inserat
     * @throws InsertionFailedException
     */
    public static void insertProduct(Product p) throws InsertionFailedException {
        int success= ProductDAO.insertProduct(p);
        if(success != 1) {
            throw new InsertionFailedException("Inserting product "+p.getName()+" -failed");
        }
    }

    /**
     * Aceasta metoda se ocupa cu stergerea unui client in functie de numele trimis ca parametru. Odata cu stergerea
     * unui produs se vor sterge toate comenzile care contin acel produs. In momentul in care stergrea esueaza, se
     * arunca o exceptie de tipul DeletionFailedException.
     * @param name numele produsului
     * @throws DeletionFailedException
     */
    public static void deleteProduct(String name) throws DeletionFailedException {
        int id_product=ProductDAO.findByName(name);
        int success1= OrderItemDAO.deleteOrderItemProduct(id_product);
        int success2= OrderDAO.deleteOrderProduct(id_product);
        int success = ProductDAO.deleteProduct(name);
        if (success1 != 1 || success2!=1) {
            throw new DeletionFailedException("Deleting product "+name+" -failed.");
        }
        if (success != 1) {
            throw new DeletionFailedException("Deleting orders containing the product "+name+" -failed.");
        }
    }

    /**
     * Aceasta metoda are rolul de a actualiza cantitatea produsului a carui nume este trimis ca parametru.
     * Cand aceasta operatie esueaza, se va arunca o exceptie de tipul UpdateFailedException.
     * @param newQ noua cantitate
     * @param name numele produsului
     * @throws UpdateFailedException
     */
    public static void updateProduct(int newQ, String name) throws UpdateFailedException {
        int success = ProductDAO.updateProduct(newQ,name);
        if (success != 1) {
            throw new UpdateFailedException("Updating product "+ name + " -failed.");
        }
    }
}
