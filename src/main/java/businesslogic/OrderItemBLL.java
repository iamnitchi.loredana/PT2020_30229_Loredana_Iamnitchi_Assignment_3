package businesslogic;

import databaseaccess.OrderItemDAO;
import databaseaccess.ProductDAO;
import exception.InsertionFailedException;
import model.Order;
import model.OrderItem;
import model.Product;

/**
 * Aceasta clasa se ocupa cu logica operatiilor folosite pe tabela OrderItem, mai exact insert.
 */


public class OrderItemBLL {

    /**
     * Aceasta metoda are rolul de a insera o comanda in tabela OrderItem. Odata cu inserarea unei
     * comenzi noi, se va actualiza cantitatea disponibila a produsului comandat si se va insera comanda
     * in tabela Order. In cazul in care inserarea comenzii esueaza se va arunca o exceptie de tipul
     * InsertionFailedException.
     * @param order comand care urmeaza a fi inserata
     * @return 1 in caz de succes, 0 in caz ca operatia esueaza
     * @throws InsertionFailedException
     */
    public static int insertOrderItem(OrderItem order) throws InsertionFailedException{
        int ok=0;
        int success= OrderItemDAO.insertOrderItem(order);
        if(success != 1) {
            throw new InsertionFailedException("Inserting order item -failed");
        }
        else {
            Product p= ProductDAO.findById(order.getId_p());
            if(order.getQuantity()<p.getQuantity()) {
                try {
                    ProductBLL.updateProduct(p.getQuantity() - order.getQuantity(), p.getName());
                    Order o = new Order(order.getId_c(), order.getId_p(), order.getQuantity(), p.getPrice());
                    OrderBLL.insertOrder(o);
                    ok = 1;
                } catch (Exception e) {
                    System.out.println("Erorr: "+ e.getMessage());
                }
            }
        }
        return ok;
    }

}
