package model;

/**
 * Clasa in care se face maparea corespunzatore tabelului Orders din baza de date.
 */
public class Order {

    private int id_c;
    private int id_p;
    private int quantity;
    private float price;

    /**
     * Constructorul clasei. Aici se initializeaza variabilele instanta ale obiectului de tip Order
     * @param id_c id-ul clientului
     * @param id_p id-ul produsului
     * @param quantity cantitatea
     * @param price pretul
     */
    public Order(int id_c, int id_p, int quantity, float price) {
        this.id_c = id_c;
        this.id_p = id_p;
        this.quantity = quantity;
        this.price = price;
    }

    /**
     * Metoda care returneaza id-ul clientului
     * @return id-ul clientului
     */
    public int getId_c() {
        return id_c;
    }

    /**
     * Metoda care returneaza id-ul produsului
     * @return id-ul produsului
     */
    public int getId_p() {
        return id_p;
    }

    /**
     * Metoda care returneaza cantitatea
     * @return cantitatea
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Metoda care returneaza pretul
     * @return pretul
     */
    public float getPrice() {
        return price;
    }

}
