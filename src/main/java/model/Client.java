package model;

/**
 * Clasa in care se face maparea corespunzatore tabelului Clients din baza de date.
 */
public class Client {

    private int id_c;
    private String name;
    private String address;

    /**
     * Constructorul clasei. Aici se initializeaza variabilele instanta ale obiectului de tip Client
     * @param id_c id-ul clientului
     * @param name numele clientului
     * @param adress adresa clientului
     */
    public Client(int id_c, String name, String adress) {
        this.id_c = id_c;
        this.name = name;
        this.address = adress;
    }

    /**
     * Metoda care returneaza id-ul clientului
     * @return id-ul clientului
     */
    public int getId_c() {
        return id_c;
    }

    /**
     * Metoda care returneaza numele clientului
     * @return numele clientului
     */
    public String getName() {
        return name;
    }

    /**
     * Metoda care returneaza adresa clientului
     * @return adresa clientului
     */
    public String getAddress() {
        return address;
    }

}
