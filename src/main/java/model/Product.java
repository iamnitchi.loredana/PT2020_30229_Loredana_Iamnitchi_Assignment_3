package model;

/**
 * Clasa in care se face maparea corespunzatore tabelului Products din baza de date.
 */
public class Product {

    private int id_p;
    private String name;
    private int quantity;
    private float price;

    /**
     * Constructorul clasei. Aici se initializeaza variabilele instanta ale obiectului de tip Product
     * @param id_p id ul produsului
     * @param name numele produsului
     * @param quantity cantitatea disponibila
     * @param price pretul pe bucata
     */
    public Product(int id_p, String name, int quantity, float price) {
        this.id_p = id_p;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    /**
     * Metoda care returneaza id-ul produsului
     * @return id-ul produsului
     */
    public int getId_p() {
        return id_p;
    }

    /**
     * Metoda care returneaza numele produsului
     * @return numele produsului
     */
    public String getName() {
        return name;
    }

    /**
     * Metoda care returneaza cantitatea
     * @return cantitatea
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Metoda care returneaza pretul
     * @return pretul
     */
    public float getPrice() {
        return price;
    }
}
