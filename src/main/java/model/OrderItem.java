package model;

/**
 * Clasa in care se face maparea corespunzatore tabelului OrderItem din baza de date.
 */
public class OrderItem {

    private int id_o;
    private int id_c;
    private int id_p;
    private int quantity;

    /**
     * Constructorul clasei. Aici se initializeaza variabilele instanta ale obiectului de tip OrderItem
     * @param id_o id ul comenzii
     * @param id_c id ul clientului
     * @param id_p id ul produsului
     * @param quantity cantitatea cumparata
     */
    public OrderItem(int id_o, int id_c, int id_p, int quantity) {
        this.id_o = id_o;
        this.id_c = id_c;
        this.id_p = id_p;
        this.quantity = quantity;
    }

    /**
     * Metoda care returneaza id-ul comenzii
     * @return id-ul comenzii
     */
    public int getId_o() {
        return id_o;
    }

    /**
     * Metoda care returneaza id-ul clientului
     * @return id-ul clientului
     */
    public int getId_c() {
        return id_c;
    }

    /**
     * Metoda care returneaza id-ul produsului
     * @return id-ul produsului
     */
    public int getId_p() {
        return id_p;
    }

    /**
     * Metoda care returneaza cantitatea
     * @return cantitatea
     */
    public int getQuantity() {
        return quantity;
    }
}
