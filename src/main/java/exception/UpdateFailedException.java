package exception;

/**
 * Clasa de tip Exception, folosita atunci cand apare o eroare la o operatie de tip Update
 */
public class UpdateFailedException extends Exception{
    /**
     * Constructorul clasei
     * @param msg mesajul care se va afisa atuni cand va fi prinsa o exceptie de acest tip
     */
    public UpdateFailedException(String msg)
    {
        super(msg);
    }
}