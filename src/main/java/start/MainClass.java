package start;

import presentation.Parser;

/**
 *  Clasa principala, in care se porneste executia programului.
 * @author Loredana Iamnitchi
 */
public class MainClass {

    /**
     * Metoda responsabila cu crearea unui obiect de tip Parser, care va invoca metoda parseFile, unde se face parsarea fisierului .txt primit ca argument.
     * @param args numele fisierului de intrare
     */
    public static void main(String[] args)
    {
        //Parser parser = new Parser("in.txt");
        //System.out.println(args[0]);
        Parser parser = new Parser(args[0]);
        parser.parseFile();
    }
}
