package databaseaccess;

import connection.ConnectionFactory;
import model.Client;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Aceasta clasa se ocupa cu diverse interogari pe tabela Clients.
 */

public class ClientDAO {

    private final static String reportStatementString = "SELECT * FROM Clients";
    private final static String findNameStatementString = "SELECT id_c FROM Clients where name=?";
    private final static String findStatementString = "SELECT * FROM Clients where id_c= ?";
    private final static String insertStatementString = "INSERT INTO Clients VALUES(?,?,?)";
    private final static String deleteStatementString = "DELETE FROM Clients WHERE name=? and address=?";

    /**
     * Aceasta metoda executa "SELECT * FROM Clients" pentru a oferi mai departe toate datele din tabela Clients.
     * @return ResultSet ul interogarii
     */
    public static ResultSet reportClients() {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement repStatement = null;
        ResultSet rs = null;
        try {
            repStatement = dbConnection.prepareStatement(reportStatementString);
            rs = repStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }


    /**
     * Aceasta metoda face o cautarea dupa nume in tabela Clients, returnand id-ul persoanei cu numele trimis ca parametru.
     * @param name numele clientului a carui id se doreste
     * @return id-ul clientului cu numele dat
     */
    public static int findByName(String name) {
        int id=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findNameStatementString);
            findStatement.setString(1, name);
            rs = findStatement.executeQuery();
            if(rs.next()) {
                id = rs.getInt("id_c");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return id;
    }

    /**
     * Aceasta metoda face o cautare dupa id in tabela Clients, returnand clientul corespunzator id ului trimis ca parametru.
     * @param clientID id ul clientului cautat
     * @return clientul cautat
     */
    public static Client findById(int clientID) {
        Client c1=null;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, clientID);
            rs = findStatement.executeQuery();
            if(rs.next()){
                String name = rs.getString("name");
                String address = rs.getString("address");
                c1 = new Client(clientID, name, address);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return c1;
    }

    /**
     * Metoda care se ocupa de inserarea unui client in tabela Clients.
     * @param client ul care se doreste a fi inserat
     * @return 1 in caz de succes, 0 in caz ca operatia esueaza
     */
    public static int insertClient(Client client) {
        int ok=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString);
            insertStatement.setInt(1, client.getId_c());
            insertStatement.setString(2, client.getName());
            insertStatement.setString(3, client.getAddress());
            insertStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ok=1;
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return ok;
    }

    /**
     * Metoda care se ocupa de stegerea unui client din Clients.
     * @param name numele clientului
     * @param address adresa clientului
     * @return 1 in caz de succes, 0 in caz ca operatia esueaza
     */
    public static int deleteClient(String name, String address) {
        int ok=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setString(1, name);
            deleteStatement.setString(2, address);
            deleteStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ok=1;
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
        return ok;
    }
}

