package databaseaccess;

import connection.ConnectionFactory;
import model.Order;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Aceasta clasa se ocupa cu diverse interogari pe tabela Orders.
 */

public class OrderDAO {

    private final static String reportStatementString = "SELECT * FROM Orders ";
    private final static String findStatementString = "SELECT * FROM Orders where id_c= ? and id_p=?";
    private final static String updateStatementString = "UPDATE Orders SET quantity=?, price=? where id_c=? and id_p=?";
    private final static String insertStatementString = "INSERT INTO Orders VALUES(?,?,?,?)";
    private final static String deleteCStatementString = "DELETE FROM Orders WHERE id_c=?";
    private final static String deletePStatementString = "DELETE FROM Orders WHERE id_p=?";

    /**
     * Aceasta metoda executa "SELECT * FROM Oders" pentru a oferi mai departe toate datele din tabela Orders.
     * @return ResultSet ul interogarii
     */
    public static ResultSet reportOrders() {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement repStatement = null;
        ResultSet rs = null;
        try {
            repStatement = dbConnection.prepareStatement(reportStatementString);
            rs = repStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    /**
     * Metoda care se ocupa de gasirea comenzii cu acelasi client si acelasi produs
     * @param clientID id ul clientului
     * @param productID id ul produsului
     * @return comanda care corespunde criteriilor de cautare
     */
    public static Order findById(int clientID, int productID) {
        Order o=null;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, clientID);
            findStatement.setLong(2, productID);
            rs = findStatement.executeQuery();
            if(rs.next()) {
                int quantity = rs.getInt("quantity");
                float price = rs.getFloat("price");
                o = new Order(clientID, productID, quantity, price);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return o;
    }

    /**
     * Metoda care se ocupa de inserarea unei comenzi in tabela Orders
     * @param o comanda care urmeaza a fi inserata
     * @return 1 in caz de succes, 0 in caz ca operatia esueaza
     */
    public static int insertOrder(Order o) {
        int ok=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        try {
            Order o1=findById(o.getId_c(), o.getId_p());
            if(o1!=null)
                updateOrder(o1.getQuantity()+o.getQuantity(), (float)(o1.getQuantity()+o.getQuantity())*o.getPrice(), o.getId_c(), o.getId_p());
            else {
                insertStatement = dbConnection.prepareStatement(insertStatementString);
                insertStatement.setInt(1, o.getId_c());
                insertStatement.setInt(2, o.getId_p());
                insertStatement.setInt(3, o.getQuantity());
                insertStatement.setFloat(4, o.getPrice()*o.getQuantity());
                insertStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ok=1;
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return ok;
    }

    /**
     * Aceasta metoda se ocupa cu actualizarea pretului si a cantitatii in cazul in care un client a cumparat
     * de 2 sau mai multe ori acelasi produs.
     * @param newQ noua cantitate
     * @param newP noul pret
     * @param id_c id-ul clientului
     * @param id_p id-ul produsului
     * @return 1 in caz de succes, 0 in caz ca operatia esueaza
     */
    public static int updateOrder(int newQ, float newP, int id_c, int id_p) {
        int ok = 1;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement updateStatement = null;
        try {
            updateStatement = dbConnection.prepareStatement(updateStatementString);
            updateStatement.setInt(1, newQ);
            updateStatement.setFloat(2, newP);
            updateStatement.setInt(3, id_c);
            updateStatement.setInt(4, id_p);
            updateStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ok = 1;
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }
        return ok;
    }

    /**
     * Metoda care se ocupa cu stergerea unei comenzi a clientului cu id dat
     * @param clientID id-ul clientului
     * @return 1 in caz de succes, 0 in caz ca operatia esueaza
     */
    public static int deleteOrderClient(int clientID) {
        int ok=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteCStatementString);
            deleteStatement.setInt(1, clientID);
            deleteStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ok=1;
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
        return ok;
    }

    /**
     * Metoda care se ocupa de stergerea unei comenzi care contine produsl dat
     * @param productID id-ul produsului
     * @return 1 in caz de succes, 0 in caz ca operatia esueaza
     */
    public static int deleteOrderProduct(int productID) {
        int ok=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deletePStatementString);
            deleteStatement.setInt(1, productID);
            deleteStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ok=1;
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
        return ok;
    }
}
