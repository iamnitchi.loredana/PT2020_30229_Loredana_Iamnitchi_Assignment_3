package databaseaccess;

import connection.ConnectionFactory;
import model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Aceasta clasa se ocupa cu diverse interogari pe tabela Products.
 */

public class ProductDAO {

    private final static String reportStatementString = "SELECT * FROM Products";
    private final static String findQStatementString = "SELECT * FROM Products where name= ?";
    private final static String findStatementString = "SELECT * FROM Products where id_p= ?";
    private final static String insertStatementString = "INSERT INTO Products VALUES(?,?,?,?)";
    private final static String deleteStatementString = "DELETE FROM Products WHERE name=?";
    private final static String updateStatementString = "UPDATE Products SET quantity=? where name=?";

    /**
     * Aceasta metoda executa "SELECT * FROM Products" pentru a oferi mai departe toate datele din tabela Products.
     * @return ResultSet ul interogarii
     */
    public static ResultSet reportProducts() {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement repStatement = null;
        ResultSet rs = null;
        try {
            repStatement = dbConnection.prepareStatement(reportStatementString);
            rs = repStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    /**
     * Metoda care se ocupa cu cautarea unui produs dupa id
     * @param productID id-ul produsului
     * @return produsul corespunzator id ului dat
     */
    public static Product findById(int productID) {
        Product prod= null;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, productID);
            rs = findStatement.executeQuery();
            while(rs.next()) {
                String name = rs.getString("name");
                int quantity = rs.getInt("quantity");
                float price = rs.getFloat("price");
                prod = new Product(productID, name, quantity, price);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return prod;
    }

    /**
     * Aceasta metoda face o cautare dupa nume in tabela Products, returnand id ul corespunzator prodului
     * cu numele trimis ca parametru.
     * @param name numele produsului
     * @return id ul produsului
     */
    public static int findByName(String name) {
        int id=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findQStatementString);
            findStatement.setString(1, name);
            rs = findStatement.executeQuery();
            if(rs.next()) {
                id = rs.getInt("id_p");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return id;
    }

    /**
     * Metoda care cauta cantitatea disponibila a prodului a carui nume este trimis ca parametru
     * @param name numele prodului
     * @return cantitatea cautata
     */
    public static int findQuantity(String name) {
        int quantity=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findQStatementString);
            findStatement.setString(1, name);
            rs = findStatement.executeQuery();
            if(rs.next()) {
                quantity = rs.getInt("quantity");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return quantity;
    }

    /**
     * Metoda care se ocupa cu inserarea unui produs in tabel
     * @param p prodului care urmeaza a fi inserat
     * @return 1 in caz de succes, 0 in caz ca operatia esueaza
     */
    public static int insertProduct(Product p) {
        int ok=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        try {
            if(findQuantity(p.getName())!=0)
                updateProduct(p.getQuantity()+findQuantity(p.getName()), p.getName());
            else{
                insertStatement = dbConnection.prepareStatement(insertStatementString);
                insertStatement.setInt(1, p.getId_p());
                insertStatement.setString(2, p.getName());
                insertStatement.setInt(3, p.getQuantity());
                insertStatement.setFloat(4, p.getPrice());
                insertStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ok=1;
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return ok;
    }

    /**
     * Metoda care are rolul de a sterge un produs din tabel in functie de numele acestuia
     * @param name numele produsului
     * @return 1 in caz de succes, 0 in caz ca operatia esueaza
     */
    public static int deleteProduct(String name) {
        int ok=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setString(1, name);
            deleteStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ok=1;
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
        return ok;
    }

    /**
     * Metoda responsabila de actualizarea stocului(cantitatii disponibile) a produsului cu numele dat
     * @param newQ noua cantitate
     * @param name numele produsului
     * @return 1 in caz de succes, 0 in caz ca operatia esueaza
     */
    public static int updateProduct(int newQ, String name) {
        int ok = 0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement updateStatement = null;
        try {
            updateStatement = dbConnection.prepareStatement(updateStatementString);
            updateStatement.setInt(1, newQ);
            updateStatement.setString(2, name);
            updateStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ok = 1;
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }
        return ok;
    }
}
