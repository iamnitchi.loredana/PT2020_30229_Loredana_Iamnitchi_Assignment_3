package databaseaccess;

import connection.ConnectionFactory;
import model.OrderItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Aceasta clasa se ocupa cu diverse interogari pe tabela OrderItem.
 */

public class OrderItemDAO {

    private final static String insertStatementString = "INSERT INTO OrderItem VALUES(?,?,?,?)";
    private final static String billStatementString = "SELECT * FROM OrderItem where id_o=?";
    private final static String deleteCStatementString = "DELETE FROM OrderItem WHERE id_c=?";
    private final static String deletePStatementString = "DELETE FROM OrderItem WHERE id_p=?";

    /**
     * Metoda care executa "SELECT * FROM OrderItem where id_o=?" pentru a oferi mai departe datele necesare pentru generarea unui bon
     * @param orderID id ul comenzii pentru care se doreste bon
     * @return ResultSet ul interogarii
     */
    public static ResultSet billOrderItem(int orderID) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement billStatement = null;
        ResultSet rs = null;
        try {
            billStatement = dbConnection.prepareStatement(billStatementString);
            billStatement.setInt(1, orderID);
            rs = billStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    /**
     * Metoda care se ocupa cu inserarea unei comenzi in tabela OrderItem
     * @param o comanda care urmeaza a fi inserata
     * @return 1 in caz de succes, 0 in caz ca operatia esueaza
     */
    public static int insertOrderItem(OrderItem o) {
        int ok=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString);
            insertStatement.setInt(1, o.getId_o());
            insertStatement.setInt(2, o.getId_c());
            insertStatement.setInt(3, o.getId_p());
            insertStatement.setInt(4, o.getQuantity());
            insertStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ok=1;
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return ok;
    }

    /**
     * Metoda care se ocupa de stergerea unei comenzi a clientului dat
     * @param clientID id-ul clientului
     * @return 1 in caz de succes, 0 in caz ca operatia esueaza
     */
    public static int deleteOrderItemClient(int clientID) {
        int ok=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteCStatementString);
            deleteStatement.setInt(1, clientID);
            deleteStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ok=1;
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
        return ok;
    }

    /**
     * Metoda care se ocupa de stergerea unei comenzi care contine produsul dat
     * @param productID id ul produsului
     * @return 1 in caz de succes, 0 in caz ca operatia esueaza
     */
    public static int deleteOrderItemProduct(int productID) {
        int ok=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deletePStatementString);
            deleteStatement.setInt(1, productID);
            deleteStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ok=1;
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
        return ok;
    }
}
