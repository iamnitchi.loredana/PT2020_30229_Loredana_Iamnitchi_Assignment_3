package presentation;

import businesslogic.ClientBLL;
import businesslogic.OrderItemBLL;
import businesslogic.ProductBLL;
import databaseaccess.ClientDAO;
import databaseaccess.OrderDAO;
import databaseaccess.OrderItemDAO;
import databaseaccess.ProductDAO;
import model.Client;
import model.OrderItem;
import model.Product;
import java.io.*;

/**
 * Clasa in care se face parsarea fisierului .txt
 */
public class Parser {

    private String fileIn;
    private static int id_c=1;
    private static int id_p=1;
    private static int id_o=1;

    /**
     * Constructorul clasei
     * @param fileIn numele fisierului care va fi parsat
     */
    public Parser(String fileIn) {
        this.fileIn = fileIn;
    }

    /**
     * Metoda principala in care se citeste linie cu linie fisierul urmand
     * ca sa apeleze alte metode in functie de primul cuvant(care reprezinta operatia)
     */
    public void parseFile()
    {
        try {
            FileReader myFile= new FileReader(fileIn);
            BufferedReader myReader = new BufferedReader(myFile);
            String line=myReader.readLine();
            while(line!=null) {
                String s1=line.substring(0, 6);
                s1=s1.toLowerCase();
                if(s1.equals("insert"))
                    isInsert(line);
                if(s1.equals("report"))
                    isReport(line);
                if(s1.equals("delete"))
                    isDelete(line);
                if(s1.equals("order:"))
                    isOrder(line);
                line = myReader.readLine();
            }
            myReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda corespunzatoare operatiei Insert. Aceasta cauta tabelul pe care se doreste a fi
     * efectuata operatia, creeand un obiect de tipul corespunzator si apeland metoda de insert
     * corespunzatoare din ClientBLL, respectiv ProductBLL
     * @param line linia curenta din fisier
     */
    private void isInsert(String line){
        int idx=line.indexOf(":");
        String s=line.substring(7,idx);
        s=s.toLowerCase();
        if(s.equals("client")) {
            int i1 = line.indexOf(",");
            Client c=new Client(id_c, line.substring(15, i1), line.substring(i1 + 2));
            try{
                ClientBLL.insertClient(c);
                id_c++;
            } catch (Exception e) {
                System.out.println("Erorr: "+ e.getMessage());
            }
        }
        if(s.equals("product")) {
            int i1 = line.indexOf(",");
            String s1=line.substring(i1+2);
            int i2=s1.indexOf(",");
            String name=line.substring(16, i1);
            int quantity=Integer.parseInt(s1.substring(0,i2));
            float price=Float.parseFloat(s1.substring(i2+2));
            Product p=new Product(id_p,name,quantity, price);
            try {
                ProductBLL.insertProduct(p);
                id_p++;
            } catch (Exception e) {
                System.out.println("Erorr: "+ e.getMessage());
            }
        }
    }

    /**
     * Metoda corespunzatoare operatiei Report. Apeleaza metodele corespunzatoare din GeneratePDF.
     * @param line linia curenta din fisier
     */
    private void isReport(String line) {
        String s=line.substring(7);
        s=s.toLowerCase();
        if(s.equals("client"))
            GeneratePDF.generatePdfClient(ClientDAO.reportClients());
        if(s.equals("product"))
            GeneratePDF.generatePdfProducts(ProductDAO.reportProducts());
        if(s.equals("order"))
            GeneratePDF.generatePdfOrders(OrderDAO.reportOrders());
    }

    /**
     * Metoda corespunzatoare operatiei Delete. Aceasta cauta tabelul pe care se doreste a fi
     * efectuata operatia  si apeland metoda de insert corespunzatoare din ClientBLL, respectiv ProductBLL.
     * @param line linia curenta din fisier
     */
    private void isDelete(String line) {
        int idx=line.indexOf(":");
        String s=line.substring(7,idx);
        s=s.toLowerCase();
        if(s.equals("client")) {
            int i1 = line.indexOf(",");
            String name=line.substring(idx+2, i1);
            String address=line.substring(i1 + 2);
            try{
                ClientBLL.deleteClient(name, address);
            } catch (Exception e) {
                System.out.println("Erorr: "+ e.getMessage());
            }
        }
        if(s.equals("product")) {
            int i1 = line.indexOf(",");
            String name=line.substring(idx+2);
            try{
                ProductBLL.deleteProduct(name);
            } catch (Exception e) {
                System.out.println("Erorr: "+ e.getMessage());
            }
        }
    }

    /**
     * Metoda corespunzatoare operatiei Order. Aceasta creeaza un obiect de tip OrderItem, incercand sa-l insereze in tabela corespunzatoare.
     * Daca aceasta operatie are loc cu succes, se va genera bonul, in caz contrar se va genera un pdf cu un mesaj de eroare.
     * @param line linia curenta din fisier
     */
    private void isOrder(String line) {
        int i1 = line.indexOf(",");
        String s1=line.substring(i1+2);
        int i2=s1.indexOf(",");
        String clientName=line.substring(7, i1);
        int idClient=ClientDAO.findByName(clientName);
        String productName= s1.substring(0,i2);
        int idProduct=ProductDAO.findByName(productName);
        int quantity=Integer.parseInt(s1.substring(i2+2));
        OrderItem o=new OrderItem(id_o,idClient,idProduct,quantity);
        try{
            if(OrderItemBLL.insertOrderItem(o)==0)
                GeneratePDF.generatePdfError();
            else
                GeneratePDF.generatePdfBill(OrderItemDAO.billOrderItem(id_o));
            id_o++;
        } catch (Exception e) {
            System.out.println("Erorr: "+ e.getMessage());
        }
    }
}
