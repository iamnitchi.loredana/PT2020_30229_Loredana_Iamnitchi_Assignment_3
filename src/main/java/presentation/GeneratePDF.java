package presentation;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import databaseaccess.ClientDAO;
import databaseaccess.ProductDAO;
import model.Client;
import model.Product;
import java.io.FileOutputStream;
import java.sql.ResultSet;

/**
 * Clasa generatoare de pdf. Se genereaza un fisier pdf cu continutul tabelului corespunzator, totodata aici se genereaza si
 *  pdf ul corespunzator bonului, precum si cel de eroare, in cazul in care comanda nu se poate efectua.
 */
public class GeneratePDF {
    private static int nrPdfC;
    private static int nrPdfP;
    private static int nrPdfO;
    private static int nrPdfB;
    private static int nrPdfErr;

    /**
     * Metoda responsabila generarii raportului in format pdf pentru tabela Clients
     * @param rs rezulatul interogarii "SELECT * FROM Clients"
     */
    public static void generatePdfClient(ResultSet rs){
        nrPdfC++;
        try {
            String client_file = "D:\\Facultate\\An2_2\\TP\\Assignment3\\pdf\\clients"+nrPdfC+".pdf";
            Document doc = new Document();
            PdfWriter.getInstance(doc, new FileOutputStream(client_file));
            doc.open();
            PdfPTable tableClients = new PdfPTable(2);
            PdfPCell c1 = new PdfPCell(new Phrase("Name"));
            tableClients.addCell(c1);
            PdfPCell c2 = new PdfPCell(new Phrase("Address"));
            tableClients.addCell(c2);
            tableClients.setHeaderRows(1);
            while(rs.next()){
                tableClients.addCell(rs.getString("name"));
                tableClients.addCell(rs.getString("address"));
            }
            doc.add(tableClients);
            doc.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Metoda responsabila generarii raportului in format pdf pentru tabela Products
     * @param rs rezulatul interogarii "SELECT * FROM Products"
     */
    public static void generatePdfProducts(ResultSet rs){
        nrPdfP++;
        try {
            String product_file = "D:\\Facultate\\An2_2\\TP\\Assignment3\\pdf\\products"+nrPdfP+".pdf";
            Document doc = new Document();
            PdfWriter.getInstance(doc, new FileOutputStream(product_file));
            doc.open();
            PdfPTable tableProducts = new PdfPTable(3);
            PdfPCell c1 = new PdfPCell(new Phrase("Name"));
            tableProducts.addCell(c1);
            PdfPCell c2 = new PdfPCell(new Phrase("Quantity"));
            tableProducts.addCell(c2);
            PdfPCell c3 = new PdfPCell(new Phrase("Price"));
            tableProducts.addCell(c3);
            tableProducts.setHeaderRows(1);
            while(rs.next()){
                tableProducts.addCell(rs.getString("name"));
                tableProducts.addCell(rs.getString("quantity"));
                tableProducts.addCell(rs.getString("price"));
            }
            doc.add(tableProducts);
            doc.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Metoda responsabila generarii raportului in format pdf pentru tabela Orders
     * @param rs rezulatul interogarii "SELECT * FROM Orders"
     */
    public static void generatePdfOrders(ResultSet rs){
        nrPdfO++;
        try {
            String order_file = "D:\\Facultate\\An2_2\\TP\\Assignment3\\pdf\\orders"+nrPdfO+".pdf";
            Document doc = new Document();
            PdfWriter.getInstance(doc, new FileOutputStream(order_file));
            doc.open();
            PdfPTable tableOrders = new PdfPTable(4);
            PdfPCell c1 = new PdfPCell(new Phrase("Client name"));
            tableOrders.addCell(c1);
            PdfPCell c2 = new PdfPCell(new Phrase("Product name"));
            tableOrders.addCell(c2);
            PdfPCell c3 = new PdfPCell(new Phrase("Quantity purchased"));
            tableOrders.addCell(c3);
            PdfPCell c4 = new PdfPCell(new Phrase("Price"));
            tableOrders.addCell(c4);
            tableOrders.setHeaderRows(1);
            while(rs.next()){
                Client client=ClientDAO.findById(rs.getInt("id_c"));
                Product product=ProductDAO.findById(rs.getInt("id_p"));
                tableOrders.addCell(client.getName());
                tableOrders.addCell(product.getName());
                tableOrders.addCell(rs.getString("quantity"));
                tableOrders.addCell(rs.getString("price"));
            }
            doc.add(tableOrders);
            doc.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Metoda responsabila generarii raportului in format pdf pentru generarea bonului corespunzator unei comenzi
     * @param rs rezulatul interogarii "SELECT * FROM OrderItem where id_o=?"
     */
    public static void generatePdfBill(ResultSet rs){
        nrPdfB++;
        try {
            String bill_file = "D:\\Facultate\\An2_2\\TP\\Assignment3\\pdf\\bill"+nrPdfB+".pdf";
            Document doc = new Document();
            PdfWriter.getInstance(doc, new FileOutputStream(bill_file));
            doc.open();
            while(rs.next()) {
                int id_c = rs.getInt("id_c");
                int id_p = rs.getInt("id_p");
                int q = rs.getInt("quantity");
                Client client = ClientDAO.findById(id_c);
                Product product = ProductDAO.findById(id_p);
                Paragraph p1=new Paragraph(new Phrase("Client: "+client.getName()));
                Paragraph p2=new Paragraph(new Phrase("Product:  "+product.getName()));
                Paragraph p3=new Paragraph(new Phrase("Quantity: " +q));
                Paragraph p4=new Paragraph(new Phrase("Price: "+ (float)product.getPrice()*q));
                doc.add(p1);
                doc.add(p2);
                doc.add(p3);
                doc.add(p4);
            }
            doc.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     *  Metoda responsabila generarii unui fisier in format pdf care contine un mesaj de eroare in cazul in
     *  care cantitatea disponibila este mai mica decat cantitatea comandata
     */
    public static void generatePdfError()
    {
        nrPdfErr++;
        try {
            String error_file = "D:\\Facultate\\An2_2\\TP\\Assignment3\\pdf\\error"+nrPdfErr+".pdf";
            Document doc = new Document();
            PdfWriter.getInstance(doc, new FileOutputStream(error_file));
            doc.open();
            Paragraph paragraph=new Paragraph("Insufficient stock");
            doc.add(paragraph);
            doc.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
